#!/bin/bash

MYSQL=`which mysql`
EXPECTED_ARGS=2

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: $0 dbuser dbpass"
  exit $E_BADARGS
fi

Q1="CREATE USER '$1'@'%' IDENTIFIED BY '$2';"
Q2="GRANT ALL PRIVILEGES ON *.* TO '$1'@'%'  WITH GRANT OPTION;"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"

$MYSQL -uroot -p -e "$SQL"

#########################
# sudo ./mysql_grant_user.sh amy hello123
#########################