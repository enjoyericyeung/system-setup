#! /bin/bash
wget https://dl.grafana.com/oss/release/grafana_5.4.2_amd64.deb
sudo apt-get install -y adduser libfontconfig
sudo dpkg -i grafana_5.4.2_amd64.deb

#######
# add "deb https://packages.grafana.com/oss/deb stable main" to file /etc/apt/sources.list.d/grafana.list
echo "deb https://packages.grafana.com/oss/deb stable main" >> /etc/apt/sources.list.d/grafana.list

curl https://packages.grafana.com/gpg.key | sudo apt-key add -	


sudo apt-get update
sudo apt-get install grafana

# installation completed


#################
# check status
# sudo service grafana-server status


##################
# visit grafana 
# http://localhost:3000/


##################
# default user admin: 
# admin/admin

#################
# create user at [Configuration] -> [Server Admin]