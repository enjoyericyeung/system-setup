## install postgresql

```bash
sudo apt update
sudo apt install postgresql postgresql-contrib
```

Upon installation, Postgres is set up to use ident authentication, meaning that it associates Postgres roles with a matching Unix/Linux system account. If a role exists within Postgres, a Unix/Linux username with the same name is able to sign in as that role.

The installation procedure created a user account called postgres that is associated with the default Postgres role. In order to use Postgres, you can log into that account.

### Switch over to the postgres account on your server by typing:

```bash
# switch postgresql ac
sudo -i -u postgres

# access postgresql
psql

# exit psql
postgres=# \q

```

## Setup for sonarqube

### Change the password for postgres user

```
$ sudo -i -u postgres

postgres@ericubn:~$ createuser sqube

postgres=# ALTER USER sqube WITH ENCRYPTED password '123456';

postgres=# CREATE DATABASE sqube OWNER sqube;

postgres=# \q


```